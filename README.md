[![build status](https://gitlab.com/adin/journal-list/badges/master/build.svg)](https://gitlab.com/adin/journal-list/commits/master)

# About

These files hold the names of journals and conferences related to Computer Science and Engineering. The main idea is to extend IEEEtran abbreviation and full lists to other publishers, and to conferences.

I work mostly with venues related to Computer Vision, Image Processing, and Machine Learning. So, these venues are updated and checked often. However, please feel free to help by adding more venues related to computer science.

The data is stored in several `.yaml` files and converted into two `.bib` files that contain the `bibstrings` that should be imported.

## Warning

When in doubt, I followed the IEEE rules (as much as possible) to create the abbreviations of the names. However, some publishers may have different rules.

So, please double check the first time you are using this list, and if you find an error please report it (see [how to contribute](CONTRIBUTING.md)).

# Usage

Include the journal lists (`full.bib` or `abrv.bib`) with

```tex
\bibliographystyle{your_bst_style}
\bibliography{full,your_bib_file}
```

where the names in the `.bib` database entries use the strings defined in these lists. e.g.,

```tex
journal = IEEE_J_AC,
```

will produce "{IEEE} Transactions on Automatic Control".

# Install

## Download

You can download the [latest artifacts](https://gitlab.com/adin/journal-list/builds/artifacts/master/download?job=build) that should contain a `full.bib` and `abrv.bib` files, and extract the files in your local `texmf` tree. For example,

```bash
mkdir -p ~/texmf/bibtex/bib
cd ~/texmf/bibtex/bib
wget https://gitlab.com/adin/journal-list/builds/artifacts/master/download?job=build -O artifacts.zip
unzip artifacts.zip -d journal-list
rm artifacts.zip
```

## Clone

You can install this file by copying them into your local `texmf` tree or any place where LaTeX can find them. 

For example you can clone the directory in the following path

```bash
mkdir -p ~/texmf/bibtex/bib
cd ~/texmf/bibtex/bib
git clone git@gitlab.com:adin/journal-list.git
```

Once you have the files you need to build them. Note that the `convert.py` script depends on `PyYaml` library. So you need to install it using `pip`.

```bash
pip install pyyaml
cd journal-list
make all
```

The `Makefile` that comes with the project has a target (`all`) to construct the list of names (abbreviated and full) from all the set of `.yaml` files. In case you want a subset of the files, you can manually run the `convert.py` script

```bash
python convert.py f.yaml g.yaml h.yaml
```

Note that the use of `python3` is strongly recommended (as there is no test against `python2`).