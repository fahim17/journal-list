# How to contribute

The list of journals is growing and changing. If you want to add your own venues, feel free to fork and send a merge request for your data.

The same goes if you find an error and have a fix.

Any help is appreciated.

## Merge request

The workflow to make a merge request is as follows:
1. Fork the project.
1. Create a feature branch, branch away from `master`.
1. Write your data, code, or improvements.
1. Make sure you [squash your commits](https://git-scm.com/book/en/Git-Tools-Rewriting-History#Squashing-Commits).
1. Push your commits to your fork.
1. Submit a mergue (MR) request to the `master` branch.
1. The title should describe the changes you make.
1. If there are relevant issues, please link them in the MR description and comment in them to link back to the MR.
1. Be available and prepare to answer questions and incorporate feedback as needed.

# Files

The names (full and abbreviated) of the venues are stored in different files for maintaining purposes. Right now, the files follow the convention `publisher-j.yaml` where `publisher` is a short version of the publisher's name, and there are two files that contain conferences (`confs.yaml`) and workshops (`workshops.yaml`) without any further division. (Probably as the list grows, dividing them into organizing bodies will be a good idea.)

Each `.yaml` file is a set of records with three fields `key`, `full`, and `abrv` that corresponds to the `bibstring` key to be used in the documents written with these strings, the full name of the venue or publication, and the abbreviated version of it.

The structure of the files is:

```yaml
# Name of publisher or contents of the current file

- key: PUB_J_NAME
  full: Full Name of the Publication
  abrv: Full. Name PUb.

- key: PUB_C_NAME
  full: Transactions on Publisher's Names
  abrv: Trans. Pub. Names
```

# Conventions

The name of a venue or publication is `<PUBLISHER>_<T>_<ACRONYM>` where
- `<PUBLISHER>` are few letters of the publisher's name or the publisher's acronym (when available)
- `<T>` is the type of the venue 
  - `J`: journal
  - `C`: conference
  - `W`: workshop
- `<ACRONYM>` is the acronym for the venue.

## Publishers' Acronyms

Existing publishers are:
- ACM: `ACM`
- Acoustical Society of America (ASA): `ASA`
- Canadian Center of Science and Education: `CCSE`
- Elsevier: `ELS`
- Electronics and Telecommunications Research Institute: `ETRI`
- Journal of Machine Learning Research: `JMLR`
- IEEE: `IEEE`
- MIT Press: `MIT`
- Multidisciplinary Digital Publishing Institute: `MDIP`
- Now Publishers: `NP`
- Science and Information Organization: `SAI`
- Springer: `SPR`
- Taylor and Francis Online: `TFO`
- Wiley Online Library: `WOL`

## Venues' Acronyms 

In general, use the acronym for the venue. If it does not exist use the main letters of the name, and avoid including auxiliary words, i.e., just the nouns in the name.

## Abbreviations of Venues

For the abbreviations use the standard abbreviation of the journal. When in doubt you can use the existing abbreviations on the list to write yours.


