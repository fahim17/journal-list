
PYTHON := python3

SOURCEDIR := .
SOURCES := $(shell find $(SOURCEDIR) -name '*.yaml')


all:
	$(PYTHON) convert.py $(SOURCES)


.PHONY: all